﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Grades.DataModel
{
    public class GradeValidation
    {
        public void ValidateAssessmentDate(DateTime assessmentDate)
        {
            if (assessmentDate > DateTime.Now)
            {
                throw new ArgumentOutOfRangeException("Assessment Date", "Assessment date must be now or before now");
            }
        }

        public void ValidateAssessmentGrade(string assessment)
        {
            Match matchGrade = Regex.Match(assessment, @"^[A-E][+-]?$");

            if (!matchGrade.Success)
            {
                throw new ArgumentOutOfRangeException("Assessment", "Assessment must be A+ to E-");
            }
        }
    }
}
